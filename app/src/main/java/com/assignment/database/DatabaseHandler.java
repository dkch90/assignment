package com.assignment.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.assignment.model.Result;

import java.util.ArrayList;
import java.util.List;

import static com.assignment.database.DatabaseUtil.DB_NAME;
import static com.assignment.database.DatabaseUtil.DB_VERSION;
import static com.assignment.database.DatabaseUtil.DELETED_TABLE;
import static com.assignment.database.DatabaseUtil.NAME;
import static com.assignment.database.DatabaseUtil.UID;
import static com.assignment.database.DatabaseUtil.USER_TABLE;

/**
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public class DatabaseHandler {
    private static DatabaseHandler sObj;
    private SQLiteDatabase mDatabase;

    private DatabaseHandler(Context context) {
        Database database = new Database(context, DB_NAME, null, DB_VERSION);
        mDatabase = database.getWritableDatabase();
    }

    public static DatabaseHandler getInstance(Context context) {
        if (sObj == null) {
            sObj = new DatabaseHandler(context);
        }
        return sObj;
    }

    /**
     * insert contact list
     *
     * @param contactList contact list
     */
    public List<Result> insertContact(List<Result> contactList) {
        mDatabase.beginTransaction();
        try {
            for (Result result : contactList) {
                if (!isDeleted(result.getUid())) {
                    ContentValues values = new ContentValues();
                    values.put(NAME, result.getName());
                    values.put(UID, result.getUid());
                    mDatabase.insert(USER_TABLE, null, values);
                }
            }
            mDatabase.setTransactionSuccessful();
        } finally {
            mDatabase.endTransaction();
        }
        return getContactList();
    }

    /***
     * insert id of deleted contact
     *
     * @param uid UID of delete contact
     */
    public void insertDeleteContact(String uid) {
        ContentValues values = new ContentValues();
        values.put(UID, uid);
        mDatabase.insert(DELETED_TABLE, null, values);
    }

    /**
     * delete record based on uid
     *
     * @param uid UID
     */
    public long deleteContact(String uid) {
        insertDeleteContact(uid);
        return mDatabase.delete(USER_TABLE, UID + "=" + uid, null);
    }

    public boolean isDeleted(String uid) {
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + DELETED_TABLE + " WHERE " + UID + "='" + uid + "';", null);
        if (cursor != null) {
            if (cursor.moveToNext()) {
                return true;
            }
            cursor.close();
            return false;
        } else {
            return false;
        }
    }

    public List<Result> getContactList() {
        List<Result> resultList = new ArrayList<>();
        String[] columns = {BaseColumns._ID, NAME, UID};
        Cursor cursor = mDatabase.query(USER_TABLE, columns, null, null, null, null, null);
        if (cursor != null) {
            if (cursor.moveToNext()) {
                do {
                    Result result = new Result();
                    result.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                    result.setUid(cursor.getString(cursor.getColumnIndex(UID)));
                    resultList.add(result);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return resultList;
    }
}
