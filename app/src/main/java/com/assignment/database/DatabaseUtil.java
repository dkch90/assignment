package com.assignment.database;

import android.provider.BaseColumns;

/**
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public interface DatabaseUtil {
    String DB_NAME = "assignment.db";
    int DB_VERSION = 1;
    String USER_TABLE = "user";
    String NAME = "name";
    String UID = "uid";
    String DELETED_TABLE = "deleted_user";

    String USER_TABLE_QUERY = "CREATE TABLE " + USER_TABLE + "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME + " VARCHAR2(100), " + UID + " VARCHAR2(50) NOT NULL UNIQUE);";
    String DELETED_TABLE_QUERY = "CREATE TABLE " + DELETED_TABLE + "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UID + " VARCHAR2(50) NOT NULL UNIQUE);";

    int FAILURE = -1;
}
