package com.assignment.network;

import android.support.annotation.NonNull;

import com.assignment.model.Result;
import com.assignment.ui.BaseView;

import java.util.List;

/***
 * Created by Dilip.Chaudhary on 26/3/17.
 * <p>
 * This specifies the contract between the view and the presenter.
 */

public interface ContactListContract {
    interface View extends BaseView<Presenter> {
        void onContactResponse(@NonNull List<Result> list);

        void onDataSetChange(List<Result> list);
    }

    interface Presenter {
        void fetchContactList();

        void deleteRow(String uid);
    }
}
