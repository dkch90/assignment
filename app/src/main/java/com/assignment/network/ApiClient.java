package com.assignment.network;

import com.assignment.model.ContactListResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

import static com.assignment.network.ApiConst.CONTACT_LIST;

/**
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public interface ApiClient {
    @POST(CONTACT_LIST)
    Call<ContactListResponse> getContactList(@HeaderMap Map<String, String> header);
}
