package com.assignment.network;

import android.content.Context;

import com.assignment.database.DatabaseHandler;
import com.assignment.database.DatabaseUtil;
import com.assignment.model.ContactListResponse;
import com.assignment.model.Result;
import com.assignment.ui.AssignmentApplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public class ContactListPresenter implements ContactListContract.Presenter {
    private static final String TAG = "ContactListPresenter";
    private static final String SUCCESS_RESPONSE = "success";
    private ContactListContract.View mContactListView;
    private DatabaseHandler mDatabaseHandler;

    public ContactListPresenter(Context context, ContactListContract.View view) {
        mContactListView = view;
        mContactListView.setPresenter(this);
        mDatabaseHandler = DatabaseHandler.getInstance(context);
    }

    @Override
    public void fetchContactList() {
        mContactListView.showProgress();
        List<Result> resultList = mDatabaseHandler.getContactList();
        if (resultList.size() > 0) {
            mContactListView.onContactResponse(resultList);
            mContactListView.hideProgress();
        } else {
            ApiClient networkApi = AssignmentApplication.getClient();
            Call<ContactListResponse> responseCall = networkApi.getContactList(AssignmentApplication.getHeaders());
            responseCall.enqueue(new Callback<ContactListResponse>() {
                @Override
                public void onResponse(Call<ContactListResponse> call, Response<ContactListResponse> response) {
                    if (response != null && response.body() != null && SUCCESS_RESPONSE.equalsIgnoreCase(response.body().getStatus()) && response.body().getResult() != null) {
                        mContactListView.onContactResponse(mDatabaseHandler.insertContact(response.body().getResult()));
                        mContactListView.hideProgress();
                    } else {
                        mContactListView.onResponseFailure();
                        mContactListView.hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<ContactListResponse> call, Throwable t) {
                    mContactListView.onResponseFailure();
                    mContactListView.hideProgress();
                }
            });
        }
    }

    @Override
    public void deleteRow(String uid) {
        if (mDatabaseHandler.deleteContact(uid) != DatabaseUtil.FAILURE) {
            List<Result> resultList = mDatabaseHandler.getContactList();
            mContactListView.onDataSetChange(resultList);
        }
    }
}
