package com.assignment.network;

import com.assignment.BuildConfig;

/**
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public interface ApiConst {
    String BASE_URL = BuildConfig.BASE_URL;
    String CONTACT_LIST = "contactlist.php";

    int NETWORK_TIMEOUT = 60;
}
