package com.assignment.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.assignment.R;

/***
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";


    /**
     * Replace Fragment
     *
     * @param fragment       Fragment Object
     * @param bundle         Bundle to pass to the Fragment
     * @param addToBackStack boolean
     */
    protected void replaceFragment(Fragment fragment, int resId, Bundle bundle, boolean addToBackStack) {
        if (fragment != null && !fragment.isAdded()) {
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (addToBackStack) ft.addToBackStack(fragment.getClass().getSimpleName());
            Log.i(TAG, "Fragment TAG given->" + fragment.getClass().getSimpleName());
            if (resId == 0 && findViewById(R.id.container) != null)
                ft.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
            else ft.add(resId, fragment, fragment.getClass().getSimpleName());
            ft.commitAllowingStateLoss();
        }
    }

    /**
     * Fragment instance which will be replaced
     *
     * @param fragment fragment
     */
    protected void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, 0, null, false);
    }
}
