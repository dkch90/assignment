package com.assignment.ui;

import android.support.annotation.NonNull;

/**
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public interface BaseView<T> {
    /**
     * Attach presenter
     *
     * @param presenter presenter instance
     */
    void setPresenter(@NonNull T presenter);

    void onResponseFailure();

    void showProgress();

    void hideProgress();
}
