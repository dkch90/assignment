package com.assignment.ui;

import android.os.Bundle;

import com.assignment.R;
import com.assignment.network.ContactListPresenter;

/***
 * * Created by Dilip.Chaudhary on 26/3/17.
 */
public class ContactListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        ContactListFragment contactListFragment = new ContactListFragment();
        replaceFragment(contactListFragment);
        new ContactListPresenter(this, contactListFragment);
    }
}
