package com.assignment.ui;

import android.app.Application;

import com.assignment.network.ApiClient;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.assignment.network.ApiConst.BASE_URL;
import static com.assignment.network.ApiConst.NETWORK_TIMEOUT;

/**
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public class AssignmentApplication extends Application {
    private static final String TAG = "AssignmentApplication";
    //HTTP Request header
    private static final String AUTH = "Authorization";
    private static final String TOKEN = "token: c149c4fac72d3a3678eefab5b0d3a85a";

    private static Retrofit sRetrofit;

    @Override
    public void onCreate() {
        super.onCreate();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
                .build();
        sRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiClient getClient() {
        return sRetrofit.create(ApiClient.class);
    }

    public static Map<String, String> getHeaders() {
        Map<String, String> map = new HashMap<>();
        map.put(AUTH, TOKEN);
        return map;
    }
}
