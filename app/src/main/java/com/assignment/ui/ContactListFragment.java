package com.assignment.ui;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.assignment.R;
import com.assignment.model.Result;
import com.assignment.network.ContactListContract;

import java.util.List;


/***
 * Created by Dilip.Chaudhary on 26/3/17.
 * <p>
 * This class shows list of contacts having delete option
 */
public class ContactListFragment extends Fragment implements ContactListContract.View, ContactListAdapter.AdapterCallback {
    private static final String TAG = "ContactListFragment";

    private ContactListContract.Presenter mPresenter;

    private RecyclerView mRecyclerView;
    private ProgressBar mProgress;
    private ContactListAdapter mAdapter;
    private List<Result> mList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        initLayout(view);
        return view;
    }

    private void initLayout(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mProgress = (ProgressBar) view.findViewById(R.id.progress);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mPresenter.fetchContactList();
    }

    @Override
    public void setPresenter(@NonNull ContactListContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onContactResponse(@NonNull List<Result> list) {
        mList = list;
        mAdapter = new ContactListAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResponseFailure() {
        Toast.makeText(getActivity(), getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void deleteRow(String uid) {
        mPresenter.deleteRow(uid);
    }

    @Override
    public void onDataSetChange(List<Result> list) {
        mList.clear();
        mList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }
}
