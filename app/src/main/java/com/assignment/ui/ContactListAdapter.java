package com.assignment.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.R;
import com.assignment.model.Result;

import java.util.List;

/**
 * Created by Dilip.Chaudhary on 26/3/17.
 */

public class ContactListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "ContactListAdapter";
    private List<Result> mList;
    private AdapterCallback mCallback;

    public ContactListAdapter(ContactListFragment fragment, List<Result> list) {
        mList = list;
        mCallback = (AdapterCallback) fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RowViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        RowViewHolder holder = (RowViewHolder) viewHolder;
        holder.nameTxt.setText(mList.get(viewHolder.getAdapterPosition()).getName());
        holder.deleteIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.deleteRow(mList.get(viewHolder.getAdapterPosition()).getUid());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    private static class RowViewHolder extends RecyclerView.ViewHolder {
        TextView nameTxt;
        ImageView deleteIv;

        public RowViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            nameTxt = (TextView) itemLayoutView.findViewById(R.id.txt_name);
            deleteIv = (ImageView) itemLayoutView.findViewById(R.id.tv_cancel);
        }
    }

    public interface AdapterCallback {
        void deleteRow(String uid);
    }
}
